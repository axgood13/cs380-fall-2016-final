<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<title>uReview</title>
	
	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #000000;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #5f5f6d;
		font-size: 30px;
	}

	h2 {
		font-size: 50px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}
	
	div {
		margin: 5px 5px 5px 0px;
		padding: 15px 15px 15px 15px;
		border: 2px solid #D0D0D0;
		overflow: auto;
	}
	
	img {
		float: right;
		height: 250px;
		width: 250px;
	}
	
	ul {
		list-style: none;
	}
	
	#logout {
		font-size: 15px;
		float: right;
	}
	
	#back {
		font-size: 15px;
		float: left;
	}

	#body {
		margin: 0 15px 0 15px;
	}
	
	#header {
		border-bottom: 1px solid #D0D0D0;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 40px 15px;
	}
	
	a:visited {
		color:#0000FF;
	}

	</style>
</head>
	<body>	
		<?php 
			echo "<div id='header'>";
			echo "<h2 style='text-align:center'>Search Results</h2>";
			$path = base_url("User_Profile/logout");
			echo "<a id='logout' href='$path'>Logout</a>";
			$path = base_url("User_Profile/");
			echo "<a id='back' href='$path'>My Profile</a>";
			echo "</div>";
		
			$song_type = array("Track", "Artist", "Album");
			$song_elements = array($titles, $artists, $albums);
			$index = 0;
			foreach ($song_elements as $element) {
				echo "<ul>";
				echo "<h3>" . $song_type[$index] . " Name:</h3>";
				foreach ($element as $song) {
					$path = base_url("Song/search_song_clicked/" . song_string($song['title'], $song['artist'], $song['album']));
					$song['title'] = str_replace("_AND_", "&", $song['title']);
					$song['artist'] = str_replace("_AND_", "&", $song['artist']);
					$song['album'] = str_replace("_AND_", "&", $song['album']);
					echo "<li><a href='$path'><div>" . $song['title'] . " - " . $song['artist'] . ": " . $song['album'];
					echo "<img src='" . $song['image'] . "'></div></a></li>";
				}
				echo "</ul>";
				$index = $index + 1;
			}
			
			/**
			 * Turns song metadata into a url-formatted string to be posted.
			 *
			 * @param       string  title
			 * 				string 	artist
			 * 				string 	album
			 * @return		string	string
			 */
			function song_string($title, $artist, $album) {
				$string = "_T_" . $title . "_R_" . $artist . "_A_" . $album;
					
				$string = str_replace("&", "_AND_", $string);
				$string = str_replace("[", "_LBK_", $string);
				$string = str_replace("]", "_RBK_", $string);
				$string = str_replace("(", "_LPR_", $string);
				$string = str_replace(")", "_RPR_", $string);
				$string = str_replace("'", "_APP_", $string);
					
				return $string;
			}
			
		?>
	</body>
</html>