<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<title>uReview Home</title>
	
	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #000000;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #5f5f6d;
		text-align: center;
	}

	h2 {
		font-size: 30px;
	}

	#logout {
		float: right;
	}
	
	#back {
		float: left;
	}
	
	#add_song {
		float: center;
	}

	#header {
		border-bottom: 1px solid #D0D0D0;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 40px 15px;
	}
	
	h3 {
		text-align: left;
		padding: 20px 10px 20px 20px;
	}
	
	.input_box {
		height: 50px;
		width: 200px;
	}

	#body {
		margin: 0 15px 0 15px;
	}
	
	.delete {
		float: right;
	}
	
	ul {
		list-style: none;
		text-align:left;
	}
	
	#review {
		padding: 5px;
		background-color: white;
		margin: 15px;
		border-radius: 5px;
	}
	
	a:visited {
		color:#0000FF;
	}

	</style>
</head>
<body>
	<?php
		echo "<div id='header'>";
		echo "<h2>" . $title . " - " . $artist . "; (" . $album . ")</h2>";
		$path = base_url("User_Profile/logout");
		echo "<a id='logout' href='$path'>Logout</a>";
		if (!($this->Ureview_model->song_already_saved($this->aauth->get_user_id(), $song_id))) {
			$path = base_url("Song/add_song/" . song_string($title, $artist, $album, $song_id));
			echo "<a id='add_song' href='$path'>Add Song</a>";
		}
		$path = base_url("User_Profile/");
		echo "<a id='back' href='$path'>My Profile</a>";
		echo "</div>";
	
		echo "<ul>";
		foreach ($reviews as $review) {
			echo "<li>";
			$path = base_url("User_Profile/guest/" . $review['user_id']);
			echo "<div id='review'>" . $review['text'] . "<br /> by user <a href='$path'>" . $this->Ureview_model->get_email_from_id($review['user_id']) . "</a>";
			if ($this->Ureview_model->user_is_admin($this->aauth->get_user_id())) {
				$path = base_url("Song/delete_review/" . $review['review_id'] . "/" . song_string($title, $artist, $album, $song_id));
				echo "<a class='delete' href='$path'>Delete</a>";
			}
			echo "</div>";
			echo "</li>";
		}
		echo "</ul>";
		
		
		/**
		 * Turns song metadata into a url-formatted string to be posted.
		 *
		 * @param       string  title
		 * 				string 	artist
		 * 				string 	album
		 * 				int 	song_id
		 * @return		string	string
		 */
		function song_string($title, $artist, $album, $song_id) {
			$string = "_T_" . $title . "_R_" . $artist . "_A_" . $album . "_ID_" . $song_id;
			
			$string = str_replace("&", "_AND_", $string);
			$string = str_replace("[", "_LBK_", $string);
			$string = str_replace("]", "_RBK_", $string);
			$string = str_replace("(", "_LPR_", $string);
			$string = str_replace(")", "_RPR_", $string);
			$string = str_replace("'", "_APP_", $string);
			
			return $string;
		}
	?>
	<div id="form">
		<div id="form_input">
			<?php
				$song_data = array(
					'song_id' => $song_id,
					'title' => $title,
					'artist' => $artist,
					'album' => $album
				);
				echo form_open('song/create_review', '', $song_data);
						
				echo form_label('Your Review: ', 'review');
				$data= array(
				'name' => 'review',
				'placeholder' => 'Enter your review here',
				'class' => 'input_box');
				echo form_input($data);
			?>
		</div>
		
		<div id="form_button">
			<?php
				$data = array(
				'type' => 'submit',
				'value'=> 'Submit',
				'class'=> 'submit'
				);
				echo form_submit($data); 
			?>
		</div>
		
		<?php echo form_close();?>
	</div>
	
</body>
</html>