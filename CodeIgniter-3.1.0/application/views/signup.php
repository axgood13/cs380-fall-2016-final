<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<title>uReview</title>
	
	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #000000;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #5f5f6d;
		text-align: center;
	}

	h2 {
		border-bottom: 1px solid #D0D0D0;
		font-size: 30px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	</style>
</head>
	<body>
		<div class="main">
			<div id="content">
				<h3 id='form_head'>uReview Login</h3><br/>
				<?php 
					$path = base_url("Login");
					echo "<a style='float:left' href='$path'> Login </a>";
				?>
				<div id="form_input">
					<?php
						echo form_open('login/new_user');
						
						echo form_label('Email: ', 'email');
						$data= array(
						'name' => 'email',
						'placeholder' => 'Please Enter Email',
						'class' => 'input_box');
						echo form_input($data);
						echo "<br />";
						echo form_label('Password: ', 'password');
						$data= array(
						'type' => 'password',
						'name' => 'password',
						'placeholder' => 'Please Enter Password',
						'class' => 'input_box');
						echo form_input($data);
						echo "<br />";
						echo form_label('Confirm Password: ', 'confirm_password');
						$data= array(
								'type' => 'password',
								'name' => 'confirm_password',
								'placeholder' => 'Enter Password Again',
								'class' => 'input_box');
						echo form_input($data);
					?>
				</div>

				<div id="form_button">
					<?php
						$data = array(
						'type' => 'submit',
						'value'=> 'Submit',
						'class'=> 'submit'
						);
						echo form_submit($data); 
					?>
				</div>
				<?php echo form_close();?>
			</div>
		</div>
	</body>
</html>