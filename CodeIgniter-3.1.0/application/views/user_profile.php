<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<title>uReview Home</title>
	
	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #000000;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #5f5f6d;
		text-align: center;
		font-size: 15px;
	}

	h2 {
		font-size: 50px;
	}

	#my_profile {
		float: left;
	}

	#logout {
		float: right;
	}
	
	#header {
		border-bottom: 1px solid #D0D0D0;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 40px 15px;
	}
	
	#songs {
		float: left;
		margin-left: 100px;
		background-color: #F5B049;
		border-radius: 5px;
		padding: 10px 20px 10px 15px;
	}
	
	#friends {
		float: right;
		margin-right: 100px;
		background-color: #F5B049;
		border-radius: 5px;
		padding: 10px 20px 10px 15px;
	}
	
	.delete {
		float: right;
		font-size: 10px;
	}
	
	h3 {
		font-size: 30px;
		text-align: left;
		padding: 20px 10px 20px 20px;
	}

	#body {
		margin: 0 15px 0 15px;
	}
	
	ul {
		text-align:left;
	}
	
	a:visited {
		color:#0000FF;
	}

	</style>
</head>
<body>
	
	<div id="header">
		<h2>uReview</h2>
		<?php
			if ($this->session->id != $id) {
				$path = base_url("User_Profile/");
				echo "<a id='my_profile' href='$path'>My Profile</a>";
			}
			$path = base_url("User_Profile/logout");
			echo "<a id='logout' href='$path'>Logout</a>";
		?>
	</div>
	
	<div id="form_input">
		<?php
			echo form_open('song/search');
			
			echo form_label('Search For New Music: ', 'search');
			$data= array(
				'name' => 'search',
				'placeholder' => 'Search Vast Library',
				'class' => 'input_box');
			echo form_input($data);
		?>
	</div>
	<div id="form_button">
		<?php
			$data = array(
				'type' => 'submit',
				'value'=> 'Submit',
				'class'=> 'submit'
			);
			echo form_submit($data); 
		?>
	</div>
	<?php echo form_close();?>
	<?php 
		$this->load->model('Ureview_model');
		echo "<h3 style='padding-left:50px'>Hello, " . $this->session->email . "!</h3>";
		
		
		//SONGS
		if ($this->session->id == $id) {
			echo "<h3 style='text-align:center'>Welcome home. Here are your songs and friends!</h3>";
			echo "<div id='songs'>";
			echo "<p style='text-align:left'>Here are your songs:</p> <ul>";
		}
		else {
			echo "<h3 style='text-align:center'>You are visiting " . $this->Ureview_model->get_email_from_id($id) . ".</h3>";
			if (!($this->Ureview_model->is_friend($this->session->id, $id))) {
				$path = base_url("User_Profile/add_friend/" . $id);
				echo "<a href='$path'> Add Friend </a>";
			}
			echo "<div id='songs'>";
			echo "<p style='text-align:left'>Here are this user's songs:</p> <ul>";
		}
		if(sizeof($songs) > 0) {
			foreach ($songs as $song) {
				$song_data = process_metadata($song['metadata']);
				$path = base_url() . "Song/song_clicked/" . $song_data['url_string'] . "_ID_" . $song['id'];
				echo "<li><a href='$path'>" . $song_data['title'] . " - " . $song_data['artist'] . "; (" . $song_data['album'] . ")" . "</a>"; 
				if ($this->session->id == $id) {
					$path = base_url("User_Profile/delete_song/" . $song['id']); 
					echo "<a class='delete' href='$path'>Delete</a></li>";
				}
			}
		}
		else {
			if ($this->session->id == $id) {
				echo "<h5>You do not have any songs in your list.</h5>";
			}
			else {
				echo "<h5>No songs in list.</h5>";
			}
		}
		echo "</ul>";
		echo "</div>";
		
		//FRIENDS
		echo "<div id='friends'>";
		
		if ($this->session->id == $id) {
			echo "<p>Here are your friends:</p> <ul>";
		}
		else {
			echo "<p>Here are user's friends:</p> <ul>";
		}
		$friends = $this->Ureview_model->get_user_friends($id);
		if (sizeof($friends) > 0) {
			foreach ($friends as $friend) {
				echo "<li>";
				$path = base_url("User_Profile/guest/" . $friend['id']);
				echo "<a href='$path'>" . $friend['email'] . "</a>";
				$path = base_url("User_Profile/delete_friend/" . $friend['id']);
				if ($this->session->id == $id) {
					echo "<a class='delete' href='$path'>Delete</a>";
				}
				echo "</li>";
			}
		}
		else {
			echo "<p>No Friends!";
		}
		echo "</ul>";
		echo "</div>";
		
		/**
		 * This takes an encoded metadata string and turns it into a usable url string or individual song 
		 * elements. 
		 *
		 * @param       string  metadata
		 * @return		array data_array
		 * 						string 	title
		 * 						string	artist
		 * 						string	album
		 * 						string 	url_string
		 */
		function process_metadata($metadata) {
			$title_key_length = strlen("Song_Name:");
			$artist_key_length = strlen("Artist_Name:");
			$album_key_length = strlen("Album_Name:");
			
			$title = substr($metadata, $title_key_length, strpos($metadata, ";", 0) - $title_key_length);
			$title_full_len = $title_key_length + strlen($title);
			$artist = substr($metadata, $title_full_len + $artist_key_length + 1, strpos($metadata, ";", $title_full_len + 1) - ($title_full_len + $artist_key_length));
			$title_and_artist_len = $title_full_len + $artist_key_length + strlen($artist);
			$artist = substr($artist, 0, -1);
			$album = substr($metadata, $title_and_artist_len + $album_key_length + 1, -1);
			
			$string = "_T_" . $title . "_R_" . $artist . "_A_" . $album;
			
			$string = str_replace("&", "_AND_", $string);
			$string = str_replace("[", "_LBK_", $string);
			$string = str_replace("]", "_RBK_", $string);
			$string = str_replace("(", "_LPR_", $string);
			$string = str_replace(")", "_RPR_", $string);
			$string = str_replace("'", "_APP_", $string);
			
			$data_array = array(
					'title' => $title,
					'artist' => $artist,
					'album' => $album,
					'url_string' => $string
			);
			return $data_array;
		}
	?>
	
	
</body>
</html>