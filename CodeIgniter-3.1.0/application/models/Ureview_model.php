<?php
	class Ureview_model extends CI_Model {
		
		public function __construct() {
			//$userID = 51888532780980107-91C4D7F48A5228210F35CF7C6FDE54FB
			
			parent::__construct();
			$this->load->library('session');
			include("php-gracenote/Gracenote.class.php");
		}
		
		/**
		 * Determines if the login is successful
		 *
		 * @param       string email
		 * 				string password
		 * @return		boolean
		 */
		public function authenticate_user($email, $password) {
			return $this->aauth->login($email, $password);
		}
		
		/**
		 * Attempts to create a new user and returns if it was successful
		 *
		 * @param       string  email
		 * 				string 	password
		 * 				string 	confirm_password
		 * @return		boolean
		 */
		public function new_user($email, $password, $confirm_password) {
			
			return ($password == $confirm_password && $this->aauth->create_user($email, $password));
		}
		
		/**
		 * Retrieves the list of songs that the user has added to their personal song library. First retrieves
		 * all song ids attributed to the user and finds all song info attributed to those song ids. 
		 *
		 * @param       int 	user_id
		 * @return		array 	songs
		 * 					array 	Song
		 * 						int 	id
		 * 						string	ratings
		 * 						string 	metadata
		 */
		public function get_user_songs($user_id) {
			$query = $this->db->query("SELECT song_id FROM user_songs WHERE user_id='" . $user_id . "'");
			$song_ids = array();
			foreach ($query->result() as $row) { 
				if (isset($row))
				{
					array_push($song_ids, $row->song_id);
				}
			}
			
			$songs = array();
			
			foreach ($song_ids as $song_id) {
				$query = $this->db->query("SELECT * FROM songs WHERE id='" . $song_id . "'");
				$row = $query->row();
				
				if (isset($row))
				{
					$song_data = array (
						'id' => $row->id,
						'ratings' => $row->ratings,
						'metadata' => $row->metadata
					);
				}
				array_push($songs, $song_data);
			}
			return $songs;
		}
		
		/**
		 * Fetches all reviews attributed to a particular song.
		 *
		 * @param       int 	song_id
		 * @return		array	reviews
		 * 					int		review_id
		 * 					int 	song_id
		 * 					int 	user_id
		 * 					string	text
		 */
		public function get_song_reviews($song_id) {
			$query = $this->db->query("SELECT * FROM review WHERE song_id='" . $song_id . "'");
			$reviews = array();
			foreach ($query->result() as $row) {
				$review = array(
						'review_id' => $row->id,
						'song_id' => $row->song_id,
						'user_id' => $row->user_id,
						'text' => $row->text
				);
				array_push($reviews, $review);
			}
			
			return $reviews;
		}
		
		/**
		 * Fetches ALL metadata from the GraceNote API. First, finds all songs if the search 
		 * string was looking for track titles, then artists, then albums. 
		 *
		 * @param       string  search_string
		 * 				string 	search_type
		 * @return		array 	songs
		 * 					array 	Song	
		 * 						string 	title
		 * 						string 	artist
		 * 						string 	album
		 * 						string 	image
		 */
		public function search_for_music($search_string, $search_type) {
			$clientID = '308698055';
			$clientTag = '1C1A214907C2F3C10908BEF8C02C5E44';
			$api = new Gracenote\WebAPI\GracenoteWebAPI($clientID, $clientTag);
			// 			$userID = $api->register();
			
			if ($search_type == "TITLE") {
				$results = $api->searchTrack("", "", $search_string);
			}
			elseif ($search_type == "ARTIST") {
				$results = $api->searchArtist($search_string);
			}
			elseif ($search_type == "ALBUM") {
				$results = $api->searchAlbum("", $search_string);
			}
				
			$songs = array();
			foreach ($results as $album) {
				$image = $album['album_art_url'];
				if ($image == "") {
					$image = $album['artist_image_url'];
				}
				
				$song = array(
						'title' => $album['tracks'][0]['track_title'],
						'artist' => $album['tracks'][0]['track_artist_name'],
						'album' => $album['album_title'],
						'image' => $image
				);
				array_push($songs, $song);
			}
				
			return $songs;
		}

		/**
		 * Retrieves the user email attributed to the given user id.
		 *
		 * @param       int 	user_id
		 * @return		string	email
		 */
		public function get_email_from_id($user_id) {
			$query = $this->db->query("SELECT email FROM aauth_users WHERE id='" . $user_id . "'");
			$row = $query->row();
			return $row->email;
		}
		
		/**
		 * Activated when the user writes a new review for a song. This makes a 
		 * new entry in the review table of the database.
		 *
		 * @param       int 	song_id
		 * 				int 	user_id
		 * 				string 	text
		 * @return
		 */
		public function create_new_review($song_id, $user_id, $text) {
			$data = array('song_id' => $song_id, 'user_id' => $user_id, 'text' => $text);
			$str = $this->db->insert_string('review', $data);
			$this->db->query($str);
		}
		
		/**
		 * This deletes a review from a song.
		 *
		 * @param       int 	review_id
		 * @return		
		 */
		public function delete_review($review_id) {
			$str = "DELETE FROM review WHERE id='" . $review_id . "'";
			$this->db->query($str);
		}
		
		/**
		 * This finds all users that are the friend of the given user
		 *
		 * @param       int 	user_id
		 * @return		array	friends
		 * 					array 	friend
		 * 						int 	friend_id
		 * 						string 	email
		 */
		public function get_user_friends($user_id) {
			$query = $this->db->query("SELECT friend_id FROM friends WHERE user_id='" . $user_id . "'");
			
			$friends = array();
			foreach ($query->result() as $row) {
				$friend = array(
						'id' => $row->friend_id,
						'email' => $this->get_email_from_id($row->friend_id)
				);
				array_push($friends, $friend);
			}
			
			return $friends;
		}
		
		/**
		 * Checks to see if the user is friends with the other user
		 *
		 * @param       int 	user_id
		 * 				int 	friend_id
		 * @return		boolean
		 */
		public function is_friend($user_id, $friend_id) {
			$friends = $this->get_user_friends($user_id);
			foreach($friends as $friend) {
				if ($friend['id'] == $friend_id) {
					return true;
				}
			}
			return false;
		}
		
		/**
		 * This adds a new friend to the list of friends for the given user.
		 *
		 * @param       int 	user_id
		 * 				int 	friend_id
		 * @return		
		 */
		public function add_friend($user_id, $friend_id) {
			if (!($this->is_friend($user_id, $friend_id))) {
				$data = array('user_id' => $user_id, 'friend_id' => $friend_id);
				$str = $this->db->insert_string('friends', $data);
				$this->db->query($str);
			}
		}
		
		/**
		 * This deletes a given friend from a given user's friend list
		 *
		 * @param       int 	user_id
		 * 				int 	friend_id
		 * @return		
		 */
		public function delete_friend($user_id, $friend_id) {
			$str = "DELETE FROM friends WHERE user_id='" . $user_id . "' AND friend_id='" . $friend_id . "'";
			$this->db->query($str);
		}
		
		/**
		 * Checks to see if the song is already in user's list
		 *
		 * @param       int 	user_id
		 * 				int 	song_id
		 * @return		boolean
		 */
		public function song_already_saved($user_id, $song_id) {
			$songs = $this->get_user_songs($user_id);
			foreach($songs as $song) {
				if ($song['id'] == $song_id) {
					return true;
				}
			}
			return false;
		}
		
		/**
		 * Adds a new song to a user's list of saved songs.
		 *
		 * @param       int 	user_id
		 * 				int 	song_id
		 * @return		
		 */
		public function add_song($user_id, $song_id) {
			$data = array('user_id' => $user_id, 'song_id' => $song_id);
			$str = $this->db->insert_string('user_songs', $data);
			$this->db->query($str);
		}
		
		/**
		 * Deletes a song from a user's song list.
		 *
		 * @param       int 	user_id
		 * 				int 	song_id
		 * @return		
		 */
		public function delete_song($user_id, $song_id) {
			$str = "DELETE FROM user_songs WHERE user_id='" . $user_id . "' AND song_id='" . $song_id . "'";
			$this->db->query($str);
		}
		
		/**
		 * Checks to see if the song is already in our server's database.
		 *
		 * @param       string  metadata
		 * @return		boolean
		 */
		public function song_in_db($metadata) {
			$query = $this->db->query("SELECT id FROM songs WHERE metadata='" . $metadata . "'");
			$row = $query->row();
			return isset($row);
		}
		
		/**
		 * Adds the new song to our server's database and returns the new entry's id.
		 *
		 * @param       string  metadata
		 * @return		int 	song_id
		 */
		public function add_song_to_db($metadata) {
			$data = array('metadata' => $metadata);
			$str = $this->db->insert_string('songs', $data);
			$this->db->query($str);
			
			return $this->get_song_id_from_metadata($metadata);
		}
		
		/**
		 * Retrieves song id from the metadata string.
		 *
		 * @param       string  metadata
		 * @return		int 	id
		 */
		public function get_song_id_from_metadata($metadata) {
			$query = $this->db->query("SELECT id FROM songs WHERE metadata='" . $metadata . "'");
			$row = $query->row();
			return $row->id;
		}
		
		/**
		 * Checks to see if the user is an administrator.
		 *
		 * @param       int 	user_id
		 * @return		boolean
		 */
		public function user_is_admin($user_id) {
			$query = $this->db->query("SELECT admin FROM aauth_users WHERE id='" . $user_id . "'");
			$row = $query->row();
			return $row->admin == 1;
		}
	}
?>