<?php
class Login extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Ureview_model');
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->helper('form');
	}
	
	public function index() {
		$this->load->view('login');
	}
	
	/**
	 * Triggered when the user clicks login. Checks if user login is valid 
	 * then loads the appropriate view.
	 *
	 * @param       
	 * @return
	 */
	public function login() { 
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		if ($this->Ureview_model->authenticate_user($email, $password)) {
			redirect(base_url('User_Profile'));
		}
		else {
			$this->load->view('login_incorrect');
		}
	}
	
	
	/**
	 * Triggered when the user clicks on signup. Simply loads the signup view.
	 *
	 * @param
	 * @return
	 */
	public function signup() {
		$this->load->view('signup');
	}
	
	/**
	 * Triggered when the user clicks submit in the signup form. This adds the
	 * user to the user database if their passwords are the same and they have entered
	 * an appropriate username. Loads the user profile if valid.
	 *
	 * @param
	 * @return
	 */
	public function new_user() {
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$confirm_password = $this->input->post('confirm_password');
		if ($this->Ureview_model->new_user($email, $password, $confirm_password)) {
			$this->aauth->login($email, $password);
			redirect(base_url('User_Profile'));
		}
		else {
			$this->load->view('login_incorrect');
		}
	}
}
?>