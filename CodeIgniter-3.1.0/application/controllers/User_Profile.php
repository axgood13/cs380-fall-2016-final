<?php
class User_Profile extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Ureview_model');
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('aauth');
	}

	public function index() {
		$songs = $this->Ureview_model->get_user_songs($this->aauth->get_user_id());
		$data = array(
			'id' => $this->aauth->get_user_id(),
			'songs' => $songs
		);
		$this->load->view('user_profile', $data);
	}
	
	/**
	 * This is loaded when a user is visiting a different user's profile. The user can view, but not edit
	 * the other user's lists. The friend user's profile is loaded.
	 *
	 * @param       int friend_id
	 * @return
	 */
	public function guest($friend_id) {
		$songs = $this->Ureview_model->get_user_songs($friend_id);
		$data = array(
				'id' => $friend_id,
				'songs' => $songs
		);
		$this->load->view('user_profile', $data);
	}
	
	/**
	 * Triggered when the user clicks on add friend on a different user's profile. Users can't re-add
	 * other users that they are already friends with. Reload other user's profile.
	 *
	 * @param       int friend_id
	 * @return
	 */
	public function add_friend($friend_id) {
		$this->Ureview_model->add_friend($this->aauth->get_user_id(), $friend_id);
		$this->guest($friend_id);
	}
	
	/**
	 * This is called when a user clicks delete next to a friend's email on their own profile. 
	 * It just deletes the record connecting this user to the friend from the database and reloads
	 * the current user's profile.
	 *
	 * @param       int friend_id
	 * @return
	 */
	public function delete_friend($friend_id) {
		$this->Ureview_model->delete_friend($this->aauth->get_user_id(), $friend_id);
		$this->index();
	}
	
	/**
	 * This is called when a user clicks delete next to a song in their list on their own 
	 * profile. Deletes the song from their records and reloads their home page.
	 *
	 * @param       int song_id
	 * @return
	 */
	public function delete_song($song_id) {
		$this->Ureview_model->delete_song($this->aauth->get_user_id(), $song_id);
		$this->index();
	}
	
	/**
	 * Triggered when the user clicks logout in the top right. Boots the app back to the login screen.
	 *
	 * @param
	 * @return
	 */
	public function logout() {
		redirect(base_url('Login'));
		$this->aauth->logout();
	}
}

?>