<?php
class Song extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Ureview_model');
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->helper('form');
	}

	public function index() {
		$this->load->view('song');
	}
	
	/**
	 * The main search function for the searching feature of the application. 
	 * It takes the users input and fetches metadata from the model, who gets its
	 * info from the GraceNote API. Loads the search_results view.
	 *
	 * @param
	 * @return
	 */
	public function search() {
		$search_string = $this->input->post('search');
		
		$search_types = array("TITLE", "ARTIST", "ALBUM");
		
		$titles = array();
		$artists = array();
		$albums = array();
		foreach ($search_types as $type) {
			
			$results = $this->Ureview_model->search_for_music($search_string, $type);
				
			foreach ($results as $result) {
				$song = array(
						'title' => $result['title'],
						'artist' => $result['artist'],
						'album' => $result['album'],
						'image' => $result['image']
				);
				if ($type == "TITLE") {
					array_push($titles, $song);
				}
				elseif ($type == "ARTIST") {
					array_push($artists, $song);
				}
				elseif ($type == "ALBUM") {
					array_push($albums, $song);
				}
			}
		}
		
		$data = array(
				'titles' => $titles,
				'artists' => $artists,
				'albums' => $albums
		);
		
		$this->load->view('search_results',$data);
	}
	
	
	/**
	 * When the user has created a new review. This adds the review to the review
	 * database and reloads the same view.
	 *
	 * @param       string  $letter
	 * @return
	 */
	public function create_review() {
		$text = $this->input->post('review');
		$song_id = $this->input->post('song_id');
		$title = $this->input->post('title');
		$artist = $this->input->post('artist');
		$album = $this->input->post('album');
		
		$this->Ureview_model->create_new_review($song_id, $this->session->id, $text);
		
		$song_reviews = $this->Ureview_model->get_song_reviews($song_id);
		
		$data = array(
				'song_id' => $song_id,
				'title' => $title,
				'artist' => $artist,
				'album' => $album,
				'reviews' => $song_reviews
		);
		
		$this->load->view('song', $data);
	}
	
	
	/**
	 * Triggered when the user clicks on delete on a review. Permission for this feature
	 * is only given to administrators. This deletes the record with the matching review id
	 * from the database and reloads the song view with one less review.
	 *
	 * @param       int review_id, string song_string
	 * @return
	 */
	public function delete_review($review_id, $song_string) {
		$this->Ureview_model->delete_review($review_id);
		
		$this->song_clicked($song_string);
	}
	
	public function song_clicked($song_string) {
		$title_needle_length = strlen("_T_");
		$artist_needle_length = strlen("_R_");
		$album_needle_length = strlen("_A_");
		$id_needle_length = strlen("_ID_");
	
		$song_string = str_replace("%20", " ", $song_string);
		$song_string = str_replace("_AND_", "&", $song_string);
		$song_string = str_replace("_LBK_", "[", $song_string);
		$song_string = str_replace("_RBK_", "]", $song_string);
		$song_string = str_replace("_LPR_", "(", $song_string);
		$song_string = str_replace("_RPR_", ")", $song_string);
		$song_string = str_replace("_APP_", "'", $song_string);
	
		$title = substr($song_string, $title_needle_length, strpos($song_string, "_R_", 0) - $title_needle_length);
		$title_length = strlen($title) + $title_needle_length;
		$artist = substr($song_string, strpos($song_string, "_R_", 0) + $artist_needle_length, strpos($song_string, "_A_", 0) - $title_length - $artist_needle_length);
		$artist_length = $title_length + $artist_needle_length + strlen($artist);
		$album = substr($song_string, strpos($song_string, "_A_", 0) + $album_needle_length, strpos($song_string, "_ID_", 0) - $artist_length - $album_needle_length);
		$song_id = substr($song_string, strpos($song_string, "_ID_", 0) + $id_needle_length);
	
		$song_reviews = $this->Ureview_model->get_song_reviews($song_id);
	
		$song_data = array(
				'song_id' => $song_id,
				'title' => $title,
				'artist' => $artist,
				'album' => $album,
				'reviews' => $song_reviews
		);
	
		$this->load->view('song', $song_data);
	}
	
	
	/**
	 * Triggered when the user clicks on add song with a song that they don't already
	 * have saved in their personal library. Add song to their list in the database and 
	 * again reload the song view.
	 *
	 * @param       string  song_string
	 * @return
	 */
	public function add_song($song_string) {
		
		$song_id = substr($song_string, strpos($song_string, "_ID_", 0) + strlen("_ID_"));
		
		$this->Ureview_model->add_song($this->aauth->get_user_id(), $song_id);
		
		$this->song_clicked($song_string);
	}
	
	public function search_song_clicked($song_string) {
		$title_needle_length = strlen("_T_");
		$artist_needle_length = strlen("_R_");
		$album_needle_length = strlen("_A_");
		
		$song_string = str_replace("%20", " ", $song_string);
		$song_string = str_replace("_AND_", "&", $song_string);
		$song_string = str_replace("_LBK_", "[", $song_string);
		$song_string = str_replace("_RBK_", "]", $song_string);
		$song_string = str_replace("_LPR_", "(", $song_string);
		$song_string = str_replace("_RPR_", ")", $song_string);
		$song_string = str_replace("_APP_", "'", $song_string);
		
		$title = substr($song_string, $title_needle_length, strpos($song_string, "_R_", 0) - $title_needle_length);
		$title_length = strlen($title) + $title_needle_length;
		$artist = substr($song_string, strpos($song_string, "_R_", 0) + $artist_needle_length, strpos($song_string, "_A_", 0) - $title_length - $artist_needle_length);
		$artist_length = $title_length + $artist_needle_length + strlen($artist);
		$album = substr($song_string, strpos($song_string, "_A_", 0) + $album_needle_length);
		
		$metadata = "Song_Name:" . $title . ";Artist_Name:" . $artist . ";Album_Name:" . $album . ";";
		
		if (!($this->Ureview_model->song_in_db($metadata))) {
			$song_id = $this->Ureview_model->add_song_to_db($metadata);
		}
		else {
			$song_id = $this->Ureview_model->get_song_id_from_metadata($metadata);
		}
		
		$song_reviews = $this->Ureview_model->get_song_reviews($song_id);
		
		$song_data = array(
				'song_id' => $song_id,
				'title' => $title,
				'artist' => $artist,
				'album' => $album,
				'reviews' => $song_reviews
		);
		
		$this->load->view('song', $song_data);
	}
}
?>